new Vue({
  el: '#root',
  data: {
    newtodo : "",
    todos: []
  },
  mounted: function() {
    var storageData = localStorage.getItem("todos");
    this.todos = JSON.parse(storageData);
  },
  methods: {
    addTodo: function(event) {
      event.preventDefault();
      if(this.newtodo === "") return;
      this.todos.push({
        item: this.newtodo,
        done: false,
        error: false
      });
      this.newtodo = "";
    },
    doneTodo: function(todo) {
      var index = this.todos.indexOf(todo);
      var todo = this.todos[index];
      todo.done = true;
      todo.error = false;
    },
    deleteTodo: function(todo) {
      if (todo.done === false) {
        todo.error = true;
        return;
      }
      var index = this.todos.indexOf(todo);
      this.todos.splice(index, 1);
    },
    saveTodos: function(event) {
      event.preventDefault();
      localStorage.setItem("todos", JSON.stringify(this.todos));
    }
  }
})